# OpenML dataset: Lisbon-House-Prices

https://www.openml.org/d/43660

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Explore the regression algorithm using the prices of Lisbon's houses. This dataset contains a total of 246 records. 
Content
The attributes of this dataset are:

Id: is a unique identifying number assigned to each house.
Condition: The house condition  (i.e., New, Used, As New, For Refurbishment).
PropertyType: Property type (i.e., Home, Single habitation) 
PropertySubType: Property Sub Type (i.e., Apartment, duplex, etc.)  
Bedrooms: Number of Bedrooms
Bathrooms: Number of Bathrooms
AreaNet: Net area of the house
AreaGross: Gross area of the house
Parking: Number of parking places
Latitude: Geographical Latitude
Longitude: Geographical Longitude
Country: Country where the house is located
District: District where the house is located
Municipality: Municipality where the house is located
Parish: Parish where the house is located
Price Sq. M.: Price per m in the location of the house
Price: This is our training variable and target. It is the home price.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43660) of an [OpenML dataset](https://www.openml.org/d/43660). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43660/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43660/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43660/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

